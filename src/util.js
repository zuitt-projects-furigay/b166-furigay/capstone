// CAPSTONE PROJECT
function currencyName(name){
	if(typeof name !== 'string') return undefined;
	if(name === '') return false;
}

function currencyEx(ex){
	if(typeof ex !== 'object') return undefined;
	if(ex === '') return false;
}

function currencyAlias(alias){
	if(typeof alias !== 'string') return undefined;
	if(alias === '') return false;
}




const currency = {
	alias : 'riyadh',
	name : 'Saudi Arabian Riyadh',
	ex : {
		'peso' : 0.47,
		'usd' : 0.0092,
		'won' : 10.93,
		'yuan' : 0.065
	}
}

module.exports = {
	currency,
	currencyName,
	currencyEx,
	currencyAlias
}