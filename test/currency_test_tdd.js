const { currencyName, currencyEx, currencyAlias } = require('../src/util.js');

const { expect, assert } = require('chai');

describe('Test Currency TDD', () => {
	
	describe('test_currency_name', () => {

		// Missing
		it('Test name is missing', () => {
			const name = currencyName('');
			expect(name).to.equal(false);
		})

		// Not a string
		it('Test name not a string', () => {
			const name = currencyName(1);
			expect(name).to.equal(undefined);
		})

		// Empty
		it('Test name is empty', () => {
			const name = currencyName('');
			expect(name).to.equal(false);
		})

	})

	describe('test_currency_ex', () => {

		// Missing
		it('Test ex is missing', () => {
			const ex = currencyEx('');
			expect(ex).to.equal(undefined);
		})

		// Not a object
		it('Test ex not a string', () => {
			const ex = currencyEx("peso");
			expect(ex).to.equal(undefined);
		})

		// Empty
		it('Test ex is empty', () => {
			const ex = currencyEx('');
			expect(ex).to.equal(undefined);
		})

	})

	describe('test_currency_alias', () => {

		// Missing
		it('Test alias is missing', () => {
			const alias = currencyAlias('');
			expect(alias).to.equal(false);
		})

		// Not a string
		it('Test alias not a string', () => {
			const alias = currencyAlias(1);
			expect(alias).to.equal(undefined);
		})

		// Empty
		it('Test alias is empty', () => {
			const alias = currencyAlias('');
			expect(alias).to.equal(false);
		})

	})
})