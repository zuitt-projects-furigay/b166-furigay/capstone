const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert

const http = require('chai-http');
chai.use(http);

describe("API Currency Test Suite", () => {
	// Running
	it("Test API GET Currency are running", () => {
		chai.request('http://localhost:5001').get('/currency')
		.end((err, res) => {
			expect(res).not.equal(undefined);
		})
	})

	// NAME
	// Missing
	it("Test API POST Currency returns 400 if no currency name", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			ex : {
				'peso' : 0.50
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// Not a string
	it("Test API POST Currency returns 400 if currency name is not a string", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			name : 2,
			ex : {
				'peso' : 0.50
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// Empty
	it("Test API POST Currency returns 400 if currency name is empty", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			name : '',
			ex : {
				'peso' : 0.50
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// EX
	// Missing
	it("Test API POST Currency returns 400 if no currency ex", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			name : 'Philippines'
			
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// Not an object
	it("Test API POST Currency returns 400 if currency ex is not an object", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			name : 2,
			ex : 'peso: 0.47'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// Empty
	it("Test API POST Currency returns 400 if currency ex is empty", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			name : '',
			ex : {
				
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// ALIAS
	// Missing
	it("Test API POST Currency returns 400 if no currency alias", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name : 'Philippines',
			ex : {
				'peso' : 0.50
			}
			
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// Not a string
	it("Test API POST Currency returns 400 if currency alias is not a string", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 23,
			name : 2,
			ex : {
				'peso' : 0.50
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// Empty
	it("Test API POST Currency returns 400 if currency ex is empty", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : '',
			name : 'Philippines',
			ex : {
				'peso' : 0.50
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})
	
	// Duplicate
	it("Test API POST Currency returns 400 if all fields are complete but there is a duplicate alias", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			alias : 'Pinas',
			name : 'Philippines',
			ex : {
				'peso' : 0.50
			}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// Complete
	it("Test API POST Currency returns 200 if all fields are complete and there are no duplicate", (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias : 'Pinas',
			name : 'Philippines',
			ex : {
				'peso' : 0.50
			}
		})
		.end((err, res) => {
		    expect(res.status).to.equal(400);
		    done();
		})
	})

})
