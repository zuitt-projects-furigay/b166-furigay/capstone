const { currency } = require('../src/util.js')


module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data' : {}})
	})

	app.get('/currency', (req, res) => {
		return res.send({
			currency : currency
		})
	})

	app.post('/currency', (req, res) => {

		// name
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request - missing required parameter NAME'
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request - Name has to be string'
			})
		}

		if(typeof req.body.name == ''){
			return res.status(400).send({
				'error' : 'Bad Request - You need to input name'
			})
		}

		// ex
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request - missing required parameter ex'
			})
		}

		if(typeof req.body.name !== 'object'){
			return res.status(400).send({
				'error' : 'Bad Request - ex has to be object'
			})
		}

		if(typeof req.body.ex == ''){
			return res.status(400).send({
				'error' : 'Bad Request - You need to input ex'
			})
		}

		// alias
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request - missing required parameter alias'
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request - ex has to be string'
			})
		}

		if(typeof req.body.alias == ''){
			return res.status(400).send({
				'error' : 'Bad Request - You need to input alias'
			})
		}

		return res.status(201).send(true);
	})
}